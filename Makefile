CC = gcc -Wall -c -fsanitize=address,undefined
bin/pscopy: obj/mainIO.o obj/funIO.o
	mkdir -p bin/
	mkdir -p data/test
	gcc -fsanitize=address,undefined $^ -o $@

obj/mainIO.o: src/mainIO.c
	mkdir -p obj/
	$(CC) $^ -o $@

obj/funIO.o: src/funIO.c
	$(CC) $^ -o $@

.PHONY: clean
clean:
	rm -rf bin/
	rm -rf obj/
	rm -rf data/test