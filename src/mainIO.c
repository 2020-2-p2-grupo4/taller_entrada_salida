#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

#define SIZE_BUFFER 1000

int main (int arg, char ** argv){

    // valido entrada de argumentos.
    if (arg!= 3)
    {
        printf("Numero incorrecto de argumentos\n");
        printf("Formato: ./pscopy ruta_fuente ruta_destino\n");
        exit(1);
    }

    umask(0);

    // abro archivos para escritura y lectura y valido errores
    int readDescriptor = open( argv[1], O_RDONLY , 0666);
    if (readDescriptor<0)
    {
        perror("open for read");
        exit(1);
    }
    int writeDescriptor = open(argv[2], O_WRONLY | O_CREAT | O_TRUNC, 0666);
    if (writeDescriptor<0)
    {
        perror("open for write");
        exit(1);
    }

    // creo buffer
    char buffer[SIZE_BUFFER];
    memset(buffer, 0, SIZE_BUFFER);

    // leo primeros 1000 bytes y valido
    int bytesRead = read(readDescriptor, buffer, SIZE_BUFFER);
    if (bytesRead <0)
    {
        perror("read");
        exit(1);
    }

    // creo variables para escritura
    int bytesWritten = 0;
    int bytesCopied = 0;

    // leo el resto de bytes hasta que no haya que leer, hasta que bytesRead sea 0.
    while (bytesRead != 0)
    {
        // escribo y valido la escritura.
        bytesWritten = write(writeDescriptor, buffer, bytesRead);
        if (bytesRead<0)
        {
        perror("write");
        exit(1);
        }
        bytesCopied += bytesWritten;

        // * leo los siguientes SIZE_BUFFER bytes hasta salir del while. 
        // * Valido errores de lectura.
        bytesRead = read(readDescriptor, buffer, SIZE_BUFFER);
        if (bytesRead<0)
        {
        perror("read en while");
        exit(1);
        }
    }
    close(writeDescriptor);
    close(readDescriptor);

    printf("%d bytes copiados\n", bytesCopied);
    return 0;
}